
#ifndef PHP_UCL_CONFIG_H
#define PHP_UCL_CONFIG_H

#include "php_ucl.h"
#include "ucl_util.h"

typedef struct {
#ifndef ZEND_ENGINE_3
  zend_object std;
#endif
  struct ucl_parser *parser;
  zend_bool added;
#ifdef ZEND_ENGINE_3
  zend_object std;
#endif
} php_ucl_config_t;

extern PHP_UCL_API zend_class_entry *php_ucl_config_ce;

PHP_UCL_API int php_ucl_config_class_register(TSRMLS_D);

#endif
