dnl $Id$
dnl config.m4 for extension ucl

PHP_ARG_ENABLE(ucl, whether to enable ucl support,
[  --enable-ucl            Enable ucl support])

if test "$PHP_UCL" != "no"; then

  PHP_ARG_ENABLE(ucl-embedded, whether to enable ucl embedded,
  [  --enable-ucl-embedded   Enable ucl embedded], yes, no)

  if test "$PHP_UCL_EMBEDDED" != "no"; then

    dnl Embedded ucl

    dnl Checks for header files
    AC_CHECK_HEADERS([fcntl.h unistd.h sys/types.h sys/stat.h sys/param.h sys/mman.h stdlib.h stddef.h stdarg.h stdbool.h stdint.h string.h unistd.h ctype.h errno.h limits.h libgen.h stdio.h float.h math.h])

    dnl Source ucl
    PHP_ADD_INCLUDE(libucl/include)
    PHP_ADD_INCLUDE(libucl/uthash)
    PHP_ADD_INCLUDE(libucl/klib)
    PHP_ADD_INCLUDE(libucl/src)

    LIBUCL_SOURCES="libucl/src/ucl_emitter.c libucl/src/ucl_emitter_streamline.c libucl/src/ucl_emitter_utils.c libucl/src/ucl_hash.c libucl/src/ucl_parser.c libucl/src/ucl_schema.c libucl/src/ucl_util.c libucl/src/xxhash.c"

  else

    dnl Check for ucl header
    PHP_ARG_WITH(ucl-includedir, for ucl header,
    [  --with-ucl-includedir=DIR  ucl header path], yes)

    if test "$PHP_UCL_INCLUDEDIR" != "no" && test "$PHP_UCL_INCLUDEDIR" != "yes"; then
        if test -r "$PHP_UCL_INCLUDEDIR/ucl.h"; then
            UCL_INCLUDES="$PHP_UCL_INCLUDEDIR"
        else
            AC_MSG_ERROR([Can't find ucl headers under "$PHP_UCL_INCLUDEDIR"])
        fi
    else
        SEARCH_PATH="/usr/local /usr"
        SEARCH_FOR="/include/ucl.h"
        if test -r $PHP_UCL/$SEARCH_FOR; then
            UCL_INCLUDES="$PHP_UCL/include"
        else
            AC_MSG_CHECKING([for ucl header files in default path])
            for i in $SEARCH_PATH ; do
                if test -r $i/$SEARCH_FOR; then
                    UCL_INCLUDES="$i/include"
                    AC_MSG_RESULT(found in $i)
                fi
            done
        fi
    fi

    if test -z "$UCL_INCLUDES"; then
        AC_MSG_RESULT([not found])
        AC_MSG_ERROR([Can't find ucl headers])
    fi

    PHP_ADD_INCLUDE($UCL_INCLUDES)

    dnl Check for ucl library
    PHP_ARG_WITH(ucl-libdir, for ucl library,
    [  --with-ucl-libdir=DIR   ucl library path], yes)

    LIBNAME=ucl
    AC_MSG_CHECKING([for libucl])
    AC_LANG_SAVE

    save_CFLAGS="$CFLAGS"
    ucl_CFLAGS="-I$UCL_INCLUDES"
    CFLAGS="$save_CFLAGS $ucl_CFLAGS"

    save_LDFLAGS="$LDFLAGS"
    ucl_LDFLAGS="-L$PHP_UCL_LIBDIR -l$LIBNAME"
    LDFLAGS="$save_LDFLAGS $ucl_LDFLAGS"

    AC_TRY_LINK(
    [
        #include "ucl.h"
    ],[
        struct ucl_parser *parser;
        parser = ucl_parser_new(UCL_PARSER_NO_TIME);
    ],[
        AC_MSG_RESULT(yes)
        PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $PHP_UCL_LIBDIR, UCL_SHARED_LIBADD)
        AC_DEFINE(HAVE_UCLLIB,1,[ ])
    ],[
        AC_MSG_RESULT([error])
        AC_MSG_ERROR([wrong ucl lib version or lib not found])
    ])
    CFLAGS="$save_CFLAGS"
    LDFLAGS="$save_LDFLAGS"

    PHP_SUBST(UCL_SHARED_LIBADD)
  fi

  PHP_NEW_EXTENSION(ucl, ucl.c ucl_util.c ucl_config.c ucl_validate.c $LIBUCL_SOURCES, $ext_shared,, -DZEND_ENABLE_STATIC_TSRMLS_CACHE=1)

fi
