
#ifndef PHP_UCL_UTIL_H
#define PHP_UCL_UTIL_H

#include "ucl.h"

PHP_UCL_API void php_ucl_iterate_valid_ucl(const ucl_object_t *obj, zval *zv);
PHP_UCL_API int php_ucl_parser_add_chunk_file(struct ucl_parser *parser, char *filename);

#endif /* PHP_UCL_UTIL_H */
