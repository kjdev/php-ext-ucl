
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "Zend/zend_exceptions.h"
#include "php_ucl.h"
#include "ucl_util.h"
#include "ucl_validate.h"

zend_class_entry *php_ucl_validate_ce;
static zend_object_handlers php_ucl_validate_handlers;

ZEND_EXTERN_MODULE_GLOBALS(ucl)

ZEND_BEGIN_ARG_INFO_EX(arginfo_ucl_validate___construct,
                       0, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_INFO(0, schema)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_ucl_validate_valid, 0, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, data)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_ucl_validate_get_error, 0, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()


#ifdef ZEND_ENGINE_3

static inline php_ucl_validate_t *
php_ucl_validate_fetch_object(zend_object *obj)
{
  return (php_ucl_validate_t *)
    ((char *)obj - XtOffsetOf(php_ucl_validate_t, std));
}

#define PHP_UCL_VALIDATE_P(zv) php_ucl_validate_fetch_object(Z_OBJ_P(zv));

static inline void
php_ucl_validate_free_object(zend_object *std)
{
  php_ucl_validate_t *intern;

  intern = php_ucl_validate_fetch_object(std);
  if (!intern) {
    return;
  }

  if (intern->schema) {
    ucl_object_unref(intern->schema);
  }

  if (intern->error.c) {
    smart_string_free(&intern->error);
  }

  zend_object_std_dtor(std);
}

static inline zend_object *
php_ucl_validate_create_object(zend_class_entry *ce)
{
  php_ucl_validate_t *intern;

  intern = ecalloc(1, sizeof(php_ucl_validate_t)
                   + zend_object_properties_size(ce));

  zend_object_std_init(&intern->std, ce TSRMLS_CC);
  object_properties_init(&intern->std, ce);
  rebuild_object_properties(&intern->std);

  intern->schema = NULL;
  smart_string_free(&intern->error);

  intern->std.handlers = &php_ucl_validate_handlers;

  return &intern->std;
}

#else

static inline php_ucl_validate_t *
php_ucl_validate_fetch_object(zval *zv)
{
  TSRMLS_FETCH();
  return (php_ucl_validate_t *)zend_object_store_get_object(zv TSRMLS_CC);
}

#define PHP_UCL_VALIDATE_P(zv) php_ucl_validate_fetch_object(zv);

static inline void
php_ucl_validate_free_object(void *obj TSRMLS_DC)
{
  php_ucl_validate_t *intern = (php_ucl_validate_t *)obj;

  if (!intern) {
    return;
  }

  if (intern->schema) {
    ucl_object_unref(intern->schema);
  }

  if (intern->error.c) {
    smart_str_free(&intern->error);
  }

  zend_object_std_dtor(&intern->std TSRMLS_CC);
  efree(obj);
}

static inline zend_object_value
php_ucl_validate_create_object(zend_class_entry *ce TSRMLS_DC)
{
  php_ucl_validate_t *intern;
  zend_object_value retval;

  intern = (php_ucl_validate_t *)emalloc(sizeof(php_ucl_validate_t));
  memset(intern, 0, sizeof(php_ucl_validate_t));

  intern->schema = NULL;
  smart_str_free(&intern->error);

  zend_object_std_init(&intern->std, ce TSRMLS_CC);
  object_properties_init(&intern->std, ce);

  retval.handle = zend_objects_store_put(
    intern, (zend_objects_store_dtor_t)zend_objects_destroy_object,
    (zend_objects_free_object_storage_t)php_ucl_validate_free_object,
    NULL TSRMLS_CC);
  retval.handlers = &php_ucl_validate_handlers;

  return retval;
}

#endif

PHP_UCL_METHOD(Validate, __construct)
{
  php_ucl_validate_t *intern;
  char *schema = NULL;
#ifdef ZEND_ENGINE_3
  size_t schema_len;
#else
  int schema_len;
#endif
  struct ucl_parser *parser;

  if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s",
                            &schema, &schema_len) == FAILURE) {
    return;
  }

  if (!schema || schema_len == 0) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC, "Can not Ucl\\Validate");
    return;
  }

  intern = PHP_UCL_VALIDATE_P(getThis());
  if (!intern) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC, "Can not Ucl\\Validate object");
    return;
  }

  parser = ucl_parser_new(0);

  ucl_parser_add_chunk(parser, (const unsigned char *)schema, schema_len);

  if (ucl_parser_get_error(parser)) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC,
                            "%s", ucl_parser_get_error(parser));
    ucl_parser_free(parser);
    return;
  }

  intern->schema = ucl_parser_get_object(parser);
  ucl_parser_free(parser);
}

PHP_UCL_METHOD(Validate, valid)
{
  php_ucl_validate_t *intern;
  char *str = NULL;
#ifdef ZEND_ENGINE_3
  size_t str_len;
#else
  int str_len;
#endif
  struct ucl_parser *parser;
  ucl_object_t *data;
  struct ucl_schema_error err = { 0 };

  if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s",
                            &str, &str_len) == FAILURE) {
    return;
  }

  if (!str || str_len == 0) {
    return;
  }

  intern = PHP_UCL_VALIDATE_P(getThis());
  if (!intern) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC, "Can not Ucl\\Validate object");
    return;
  }

  parser = ucl_parser_new(0);

  ucl_parser_add_chunk(parser, (const unsigned char *)str, str_len);

  if (ucl_parser_get_error(parser)) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC,
                            "%s", ucl_parser_get_error(parser));
    ucl_parser_free(parser);
    return;
  }

  data = ucl_parser_get_object(parser);
  ucl_parser_free(parser);

  if (ucl_object_validate(intern->schema, data, &err)) {
    RETVAL_TRUE;
  } else {
    RETVAL_FALSE;
#ifdef ZEND_ENGINE_3
    if (intern->error.c) {
      smart_string_free(&intern->error);
    }
    smart_string_appendl(&intern->error, err.msg, strlen(err.msg));
    smart_string_0(&intern->error);
#else
    if (intern->error.c) {
      smart_str_free(&intern->error);
    }
    smart_str_appendl(&intern->error, err.msg, strlen(err.msg));
    smart_str_0(&intern->error);
#endif
  }

  ucl_object_unref(data);
}

PHP_UCL_METHOD(Validate, getError)
{
  php_ucl_validate_t *intern;

  if (zend_parse_parameters_none() == FAILURE) {
    return;
  }

  intern = PHP_UCL_VALIDATE_P(getThis());
  if (!intern) {
    return;
  }

  if (intern->error.c) {
#ifdef ZEND_ENGINE_3
    RETVAL_STRING(intern->error.c);
#else
    RETVAL_STRING(intern->error.c, 1);
#endif
  }
}


const zend_function_entry php_ucl_validate_methods[] = {
  PHP_UCL_ME(Validate, __construct,
             arginfo_ucl_validate___construct, ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
  PHP_UCL_ME(Validate, valid, arginfo_ucl_validate_valid, ZEND_ACC_PUBLIC)
  PHP_UCL_ME(Validate, getError,
             arginfo_ucl_validate_get_error, ZEND_ACC_PUBLIC)
  PHP_UCL_MALIAS(Validate, get_error, getError,
                 arginfo_ucl_validate_get_error, ZEND_ACC_PUBLIC)
  PHP_FE_END
};

PHP_UCL_API int
php_ucl_validate_class_register(TSRMLS_D)
{
  zend_class_entry ce;

  INIT_CLASS_ENTRY(ce, ZEND_NS_NAME(PHP_UCL_NS, "Validate"),
                   php_ucl_validate_methods);

  php_ucl_validate_ce = zend_register_internal_class(&ce TSRMLS_CC);
  php_ucl_validate_ce->create_object = php_ucl_validate_create_object;

  memcpy(&php_ucl_validate_handlers, &std_object_handlers,
         sizeof(zend_object_handlers));
#ifdef ZEND_ENGINE_3
  php_ucl_validate_handlers.offset = XtOffsetOf(php_ucl_validate_t, std);
  php_ucl_validate_handlers.dtor_obj = zend_objects_destroy_object;
  php_ucl_validate_handlers.free_obj = php_ucl_validate_free_object;
#endif

  return SUCCESS;
}
