
#ifndef PHP_UCL_H
#define PHP_UCL_H

extern zend_module_entry ucl_module_entry;
#define phpext_ucl_ptr &ucl_module_entry

#define PHP_UCL_NS "Ucl"
#define PHP_UCL_VERSION "0.1.0"

#define PHP_UCL_FUNCTION(name) ZEND_FUNCTION(name)
#define PHP_UCL_FE(name, arg_info) ZEND_NS_FE(PHP_UCL_NS, name, arg_info)
#define PHP_UCL_FALIAS(name, alias, arg_info) ZEND_NS_FALIAS(PHP_UCL_NS, name, alias, arg_info)
#define PHP_UCL_METHOD(classname, name) ZEND_METHOD(Ucl_##classname, name)
#define PHP_UCL_ME(classname, name, arg_info, flags) ZEND_ME(Ucl_##classname, name, arg_info, flags)
#define PHP_UCL_MALIAS(classname, name, alias, arg_info, flags) ZEND_MALIAS(Ucl_##classname, name, alias, arg_info, flags)
#define PHP_UCL_LONG_CONSTANT(name, val) REGISTER_NS_LONG_CONSTANT(PHP_UCL_NS, name, val, CONST_CS|CONST_PERSISTENT)

#ifdef PHP_WIN32
#	define PHP_UCL_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define PHP_UCL_API __attribute__ ((visibility("default")))
#else
#	define PHP_UCL_API
#endif

#ifdef ZTS
#include "TSRM.h"
#endif

ZEND_BEGIN_MODULE_GLOBALS(ucl)
ZEND_END_MODULE_GLOBALS(ucl)

#define UCL_G(v) ZEND_MODULE_GLOBALS_ACCESSOR(ucl, v)

#if defined(ZTS) && defined(COMPILE_DL_UCL)
ZEND_TSRMLS_CACHE_EXTERN();
#endif

#endif /* PHP_UCL_H */

