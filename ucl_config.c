
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "Zend/zend_exceptions.h"
#include "php_ucl.h"
#include "ucl_util.h"
#include "ucl_config.h"

zend_class_entry *php_ucl_config_ce;
static zend_object_handlers php_ucl_config_handlers;

ZEND_EXTERN_MODULE_GLOBALS(ucl)

ZEND_BEGIN_ARG_INFO_EX(arginfo_ucl_config___construct, 0, ZEND_RETURN_VALUE, 0)
  ZEND_ARG_INFO(0, flags)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_ucl_config_set_variable, 0, ZEND_RETURN_VALUE, 2)
  ZEND_ARG_INFO(0, key)
  ZEND_ARG_INFO(0, value)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_ucl_config_add, 0, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, str)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_ucl_config_add_file, 0, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, filename)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_ucl_config_parse, 0, ZEND_RETURN_VALUE, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_ucl_config_emit, 0, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, type)
ZEND_END_ARG_INFO()


#ifdef ZEND_ENGINE_3

static inline php_ucl_config_t *
php_ucl_config_fetch_object(zend_object *obj)
{
  return (php_ucl_config_t *)((char *)obj - XtOffsetOf(php_ucl_config_t, std));
}

#define PHP_UCL_CONFIG_P(zv) php_ucl_config_fetch_object(Z_OBJ_P(zv));

static inline void
php_ucl_config_free_object(zend_object *std)
{
  php_ucl_config_t *intern;

  intern = php_ucl_config_fetch_object(std);
  if (!intern) {
    return;
  }

  if (intern->parser) {
    ucl_parser_free(intern->parser);
  }

  zend_object_std_dtor(std);
}

static inline zend_object *
php_ucl_config_create_object(zend_class_entry *ce)
{
  php_ucl_config_t *intern;

  intern = ecalloc(1, sizeof(php_ucl_config_t)
                   + zend_object_properties_size(ce));

  zend_object_std_init(&intern->std, ce);
  object_properties_init(&intern->std, ce);
  rebuild_object_properties(&intern->std);

  intern->parser = NULL;
  intern->added = 0;

  intern->std.handlers = &php_ucl_config_handlers;

  return &intern->std;
}

#else

static inline php_ucl_config_t *
php_ucl_config_fetch_object(zval *zv)
{
  TSRMLS_FETCH();
  return (php_ucl_config_t *)zend_object_store_get_object(zv TSRMLS_CC);
}

#define PHP_UCL_CONFIG_P(zv) php_ucl_config_fetch_object(zv);

static inline void
php_ucl_config_free_object(void *obj TSRMLS_DC)
{
  php_ucl_config_t *intern = (php_ucl_config_t *)obj;

  if (!intern) {
    return;
  }

  if (intern->parser) {
    ucl_parser_free(intern->parser);
  }

  zend_object_std_dtor(&intern->std TSRMLS_CC);
  efree(obj);
}

static inline zend_object_value
php_ucl_config_create_object(zend_class_entry *ce TSRMLS_DC)
{
  php_ucl_config_t *intern;
  zend_object_value retval;

  intern = (php_ucl_config_t *)emalloc(sizeof(php_ucl_config_t));
  memset(intern, 0, sizeof(php_ucl_config_t));

  zend_object_std_init(&intern->std, ce TSRMLS_CC);
  object_properties_init(&intern->std, ce);

  retval.handle = zend_objects_store_put(
    intern, (zend_objects_store_dtor_t)zend_objects_destroy_object,
    (zend_objects_free_object_storage_t)php_ucl_config_free_object,
    NULL TSRMLS_CC);
  retval.handlers = &php_ucl_config_handlers;

  return retval;
}

#endif

PHP_UCL_METHOD(Config, __construct)
{
  php_ucl_config_t *intern;
  long flags = 0;

  if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "|l",
                            &flags) == FAILURE) {
    return;
  }

  intern = PHP_UCL_CONFIG_P(getThis());
  if (!intern) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC, "Can not Ucl\\Config object");
    return;
  }

  intern->parser = ucl_parser_new(flags);
}

PHP_UCL_METHOD(Config, setVariable)
{
  php_ucl_config_t *intern;
  char *key = NULL, *value = NULL;
#ifdef ZEND_ENGINE_3
  size_t key_len = 0, value_len = 0;
#else
  int key_len = 0, value_len = 0;
#endif
  zval val;

  if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ss",
                            &key, &key_len, &value, &value_len) == FAILURE) {
    RETURN_FALSE;
  }

  if (!key || key_len == 0 || !value || value_len == 0) {
    RETURN_FALSE;
  }

  intern = PHP_UCL_CONFIG_P(getThis());
  if (!intern) {
    RETURN_FALSE;
  }

  if (intern->added) {
    php_error_docref(NULL TSRMLS_CC, E_WARNING,
                     "Run before can add a string to parse");
    RETURN_FALSE;
  }

  ucl_parser_register_variable(intern->parser, key, value);

  if (ucl_parser_get_error(intern->parser)) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC,
                            "%s", ucl_parser_get_error(intern->parser));
    return;
  }

  RETURN_TRUE;
}

PHP_UCL_METHOD(Config, add)
{
  php_ucl_config_t *intern;
  char *str = NULL;
#ifdef ZEND_ENGINE_3
  size_t str_len = 0;
#else
  size_t str_len = 0;
#endif

  if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s",
                            &str, &str_len) == FAILURE) {
    RETURN_FALSE;
  }

  if (!str || str_len == 0) {
    RETURN_FALSE;
  }

  intern = PHP_UCL_CONFIG_P(getThis());
  if (!intern) {
    RETURN_FALSE;
  }

  ucl_parser_add_chunk(intern->parser, (const unsigned char *)str, str_len);

  if (ucl_parser_get_error(intern->parser)) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC,
                            "%s", ucl_parser_get_error(intern->parser));
    return;
  }

  intern->added = 1;

  RETURN_TRUE;
}

PHP_UCL_METHOD(Config, addFile)
{
  php_ucl_config_t *intern;
  char *filename = NULL;
#ifdef ZEND_ENGINE_3
  size_t filename_len = 0;
#else
  int filename_len = 0;
#endif

  if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s",
                            &filename, &filename_len) == FAILURE) {
    RETURN_FALSE;
  }

  if (!filename || filename_len == 0) {
    RETURN_FALSE;
  }

  intern = PHP_UCL_CONFIG_P(getThis());
  if (!intern) {
    RETURN_FALSE;
  }

  if (!php_ucl_parser_add_chunk_file(intern->parser, filename)) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC,
                            "%s", ucl_parser_get_error(intern->parser));
    return;
  }

  if (ucl_parser_get_error(intern->parser)) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC,
                            "%s", ucl_parser_get_error(intern->parser));
    return;
  }

  intern->added = 1;

  RETURN_TRUE;
}

PHP_UCL_METHOD(Config, parse)
{
  php_ucl_config_t *intern;
  ucl_object_t *obj;

  if (zend_parse_parameters_none() == FAILURE) {
    return;
  }

  intern = PHP_UCL_CONFIG_P(getThis());
  if (!intern) {
    return;
  }

  obj = ucl_parser_get_object(intern->parser);

  php_ucl_iterate_valid_ucl(obj, return_value);

  ucl_object_unref(obj);
}

PHP_UCL_METHOD(Config, emit)
{
  php_ucl_config_t *intern;
  unsigned char *emitted = NULL;
  ucl_object_t *obj;
  long type = UCL_EMIT_CONFIG;

  if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "l",
                            &type) == FAILURE) {
    return;
  }

  intern = PHP_UCL_CONFIG_P(getThis());
  if (!intern) {
    return;
  }

  obj = ucl_parser_get_object(intern->parser);

  emitted = ucl_object_emit(obj, type);

  ucl_object_unref(obj);

  if (emitted) {
#ifdef ZEND_ENGINE_3
    RETVAL_STRING(emitted);
#else
    RETVAL_STRING(emitted, 1);
#endif
    free(emitted);
  } else {
    RETVAL_EMPTY_STRING();
  }
}


const zend_function_entry php_ucl_config_methods[] = {
  PHP_UCL_ME(Config, __construct,
             arginfo_ucl_config___construct, ZEND_ACC_PUBLIC | ZEND_ACC_CTOR)
  PHP_UCL_ME(Config, setVariable,
             arginfo_ucl_config_set_variable, ZEND_ACC_PUBLIC)
  PHP_UCL_ME(Config, add, arginfo_ucl_config_add, ZEND_ACC_PUBLIC)
  PHP_UCL_ME(Config, addFile, arginfo_ucl_config_add_file, ZEND_ACC_PUBLIC)
  PHP_UCL_ME(Config, parse, arginfo_ucl_config_parse, ZEND_ACC_PUBLIC)
  PHP_UCL_ME(Config, emit, arginfo_ucl_config_emit, ZEND_ACC_PUBLIC)
  PHP_UCL_MALIAS(Config, set_variable, setVariable,
                 arginfo_ucl_config_set_variable, ZEND_ACC_PUBLIC)
  PHP_UCL_MALIAS(Config, add_file, addFile,
                 arginfo_ucl_config_add_file, ZEND_ACC_PUBLIC)
  PHP_FE_END
};

PHP_UCL_API int
php_ucl_config_class_register(TSRMLS_D)
{
  zend_class_entry ce;

  INIT_CLASS_ENTRY(ce, ZEND_NS_NAME(PHP_UCL_NS, "Config"),
                   php_ucl_config_methods);
  php_ucl_config_ce = zend_register_internal_class(&ce);
  php_ucl_config_ce->create_object = php_ucl_config_create_object;

  memcpy(&php_ucl_config_handlers, &std_object_handlers,
         sizeof(zend_object_handlers));
#ifdef ZEND_ENGINE_3
  php_ucl_config_handlers.offset = XtOffsetOf(php_ucl_config_t, std);
  php_ucl_config_handlers.dtor_obj = zend_objects_destroy_object;
  php_ucl_config_handlers.free_obj = php_ucl_config_free_object;
#endif

  return SUCCESS;
}
