--TEST--
basic #13
--SKIPIF--
<?php if (!extension_loaded("ucl")) print "skip"; ?>
--FILE--
<?php
$in = __DIR__ . '/basic/13.in';
$out = array(
  'key0' => 'value_orig',
  'key1' => 'value1',
  'key2' => 'value2',
  'key3' => 'value3',
  'key_pri' => 'priority2',
  'key_trace1' => 'pri1',
  'key_trace2' => 'pri2',
);

$res = Ucl\load(file_get_contents($in));
var_dump($res === $out);

$res = Ucl\loadFile($in);
var_dump($res === $out);

$config = new Ucl\Config();
$config->add(file_get_contents($in));
$res = $config->parse();
var_dump($res === $out);

$config = new Ucl\Config();
$config->addFile($in);
$res = $config->parse();
var_dump($res === $out);
?>
--EXPECT--
bool(true)
bool(true)
bool(true)
bool(true)
