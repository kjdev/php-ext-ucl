--TEST--
basic #3
--SKIPIF--
<?php if (!extension_loaded("ucl")) print "skip"; ?>
--FILE--
<?php
$in = __DIR__ . '/basic/3.in';
$out = array(
  'packagesite' => 'http://pkg-test.freebsd.org/pkg-test/${ABI}/latest',
  'squaretest' => 'some[]value',
  'ALIAS' => array(
    'all-depends' => 'query %dn-%dv',
    'annotations' => 'info -A',
    'build-depends' => 'info -qd',
    'download' => 'fetch',
    'iinfo' => 'info -i -g -x',
    'isearch' => 'search -i -g -x',
    'leaf' => "query -e '%a == 0' '%n-%v'",
    'list' => 'info -ql',
    'origin' => 'info -qo',
    'provided-depends' => 'info -qb',
    'raw' => 'info -R',
    'required-depends' => 'info -qr',
    'shared-depends' => 'info -qB',
    'show' => 'info -f -k',
    'size' => 'info -sq',
  ),
  'repo_dirs' => array(
    '/home/bapt',
    '/usr/local/etc',
  ),
);

$res = Ucl\load(file_get_contents($in));
var_dump($res === $out);

$res = Ucl\loadFile($in);
var_dump($res === $out);

$config = new Ucl\Config();
$config->add(file_get_contents($in));
$res = $config->parse();
var_dump($res === $out);

$config = new Ucl\Config();
$config->addFile($in);
$res = $config->parse();
var_dump($res === $out);

// Register variable
$out1 = $out;
$out1['packagesite'] = 'http://pkg-test.freebsd.org/pkg-test/unknown/latest';

$config = new Ucl\Config();
$config->setVariable('ABI', 'unknown');
$config->add(file_get_contents($in));
$res = $config->parse();
var_dump($res === $out1);

$out1['packagesite'] = 'http://pkg-test.freebsd.org/pkg-test/UNKNOWN/latest';

$config = new Ucl\Config();
$config->setVariable('ABI', 'UNKNOWN');
$config->addFile($in);
$res = $config->parse();
var_dump($res === $out1);
?>
--EXPECT--
bool(true)
bool(true)
bool(true)
bool(true)
bool(true)
bool(true)
