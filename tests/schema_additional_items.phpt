--TEST--
schema additional items
--SKIPIF--
<?php if (!extension_loaded("ucl")) print "skip"; ?>
--FILE--
<?php
$schema = <<<EOT
{
  "items": [{}],
  "additionalItems": {"type": "integer"}
}
EOT;
$validate = new Ucl\Validate($schema);
var_dump($validate->valid('[ null, 2, 3, 4 ]'));
var_dump($validate->valid('[ null, 2, 3, "foo" ]'));

$schema = <<<EOT
{
  "items": {},
  "additionalItems": false
}
EOT;
$validate = new Ucl\Validate($schema);
var_dump($validate->valid('[ 1, 2, 3, 4, 5 ]'));

$schema = <<<EOT
{
  "items": [{}, {}, {}],
  "additionalItems": false
}
EOT;
$validate = new Ucl\Validate($schema);
var_dump($validate->valid('[ 1, 2, 3 ]'));
var_dump($validate->valid('[ 1, 2, 3, 4 ]'));

$schema = '{"additionalItems": false}';
$validate = new Ucl\Validate($schema);
var_dump($validate->valid('[ 1, 2, 3, 4, 5 ]'));
var_dump($validate->valid('{"foo" : "bar"}'));

$schema = '{"items": [{"type": "integer"}]}';
$validate = new Ucl\Validate($schema);
var_dump($validate->valid('[1, "foo", false]'));
?>
--EXPECT--
bool(true)
bool(false)
bool(true)
bool(true)
bool(false)
bool(true)
bool(true)
bool(true)
