--TEST--
basic #comments
--SKIPIF--
<?php if (!extension_loaded("ucl")) print "skip"; ?>
--FILE--
<?php
$in = __DIR__ . '/basic/comments.in';
$out = array(
  'obj' => array(
    'key1' => 'value',
    'key2' => '/* value',
    'key3' => 'nested',
    'key4' => 'quotes',
  ),
);

$res = Ucl\load(file_get_contents($in));
var_dump($res === $out);

$res = Ucl\loadFile($in);
var_dump($res === $out);

$config = new Ucl\Config();
$config->add(file_get_contents($in));
$res = $config->parse();
var_dump($res === $out);

$config = new Ucl\Config();
$config->addFile($in);
$res = $config->parse();
var_dump($res === $out);
?>
--EXPECT--
bool(true)
bool(true)
bool(true)
bool(true)
