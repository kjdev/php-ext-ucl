--TEST--
basic #8
--SKIPIF--
<?php if (!extension_loaded("ucl")) print "skip"; ?>
--FILE--
<?php
$in = __DIR__ . '/basic/8.in';
$out = array(
  'section1' => array(
    'blah' => array (
      'param' => 'value',
    ),
  ),
  'section2' => array(
    'test' => array(
      'key' => 'test',
      'subsection' => array(
        'testsub' => array(
          'flag' => true,
          'subsubsection' => array(
            'testsubsub1' => array(
              'testsubsub2' => array(
                'key' => array(
                  1, 2, 3,
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  'section3' => array(
    'test' => array(),
  ),
  'section4' => array(
    'foo' => array(
      'param' => 123.2,
    ),
  ),
  'array' => array(),
);

$res = Ucl\load(file_get_contents($in));
var_dump($res === $out);

$res = Ucl\loadFile($in);
var_dump($res === $out);

$config = new Ucl\Config();
$config->add(file_get_contents($in));
$res = $config->parse();
var_dump($res === $out);

$config = new Ucl\Config();
$config->addFile($in);
$res = $config->parse();
var_dump($res === $out);
?>
--EXPECT--
bool(true)
bool(true)
bool(true)
bool(true)
