--TEST--
basic #9
--SKIPIF--
<?php if (!extension_loaded("ucl")) print "skip"; ?>
--FILE--
<?php
$in = __DIR__ . '/basic/9.in';
$out = array(
  'key1' => 'value',
  'key' => 'value',
);

$res = Ucl\load(file_get_contents($in));
var_dump($res === $out);

$res = Ucl\loadFile($in);
var_dump($res === $out);

$config = new Ucl\Config();
$config->add(file_get_contents($in));
$res = $config->parse();
var_dump($res === $out);

$config = new Ucl\Config();
$config->addFile($in);
$res = $config->parse();
var_dump($res === $out);
?>
--EXPECT--
bool(true)
bool(true)
bool(true)
bool(true)
