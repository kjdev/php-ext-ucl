--TEST--
basic #2
--SKIPIF--
<?php if (!extension_loaded("ucl")) print "skip"; ?>
--FILE--
<?php
$in = __DIR__ . '/basic/2.in';
$out = array(
  'section1' => array(
    'param1' => 'value',
    'param2' => 'value',
    'section3' => array(
      'param' => 'value',
      'param2' => 'value',
      'param3' => array(
        'value1',
        'value2',
        100500,
      ),
    ),
  ),
  'section2' => array(
    'param1' => array(
      'key' => 'value',
    ),
  ),
  'key1' => 1.0,
  'key2' => 60.0,
  'key3' => 1024,
  'key4' => 5000000,
  'key5' => 0.010000,
  'key6' => 2207520000.000000,
  'key11' => 'some string',
  'key12' => '/some/path',
  'key13' => '111some',
  'key14' => 's1',
  'key15' => "\n\r123",
  'keyvar1' => '$ABItest',
  'keyvar2' => '${ABI}$ABI${ABI}${$ABI}',
  'keyvar3' => '${some}$no${}$$test$$$$$$$',
  'keyvar4' => '$ABI$$ABI$$$ABI$$$$',
);

$res = Ucl\load(file_get_contents($in));
var_dump($res === $out);

$res = Ucl\loadFile($in);
var_dump($res === $out);

$config = new Ucl\Config();
$config->add(file_get_contents($in));
$res = $config->parse();
var_dump($res === $out);

$config = new Ucl\Config();
$config->addFile($in);
$res = $config->parse();
var_dump($res === $out);


// Parser no time
$out1 = $out;
$out1['key1'] = '1s';
$out1['key2'] = '1min';
$out1['key6'] = '10y';

$res = Ucl\load(file_get_contents($in), Ucl\PARSER_NO_TIME);
var_dump($res === $out1);

$res = Ucl\loadFile($in, Ucl\PARSER_NO_TIME);
var_dump($res === $out1);

$config = new Ucl\Config(Ucl\PARSER_NO_TIME);
$config->add(file_get_contents($in));
$res = $config->parse();
var_dump($res === $out1);

$config = new Ucl\Config(Ucl\PARSER_NO_TIME);
$config->addFile($in);
$res = $config->parse();
var_dump($res === $out1);


// Register variable
$out2 = $out;
$out2['keyvar1'] = 'unknowntest';
$out2['keyvar2'] = 'unknownunknownunknown${unknown}';
$out2['keyvar3'] = '${some}$no${}$$test$$$$$$$';
$out2['keyvar4'] = 'unknown$ABI$unknown$$';

$config = new Ucl\Config();
$config->setVariable('ABI', 'unknown');
$config->add(file_get_contents($in));
$res = $config->parse();
var_dump($res === $out2);

$out2['keyvar1'] = 'UNKNOWNtest';
$out2['keyvar2'] = 'UNKNOWNUNKNOWNUNKNOWN${UNKNOWN}';
$out2['keyvar3'] = '${some}$no${}$$test$$$$$$$';
$out2['keyvar4'] = 'UNKNOWN$ABI$UNKNOWN$$';

$config = new Ucl\Config();
$config->setVariable('ABI', 'UNKNOWN');
$config->addFile($in);
$res = $config->parse();
var_dump($res === $out2);
?>
--EXPECT--
bool(true)
bool(true)
bool(true)
bool(true)
bool(true)
bool(true)
bool(true)
bool(true)
bool(true)
bool(true)