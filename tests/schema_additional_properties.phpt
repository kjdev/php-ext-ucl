--TEST--
schema additional properties
--SKIPIF--
<?php if (!extension_loaded("ucl")) print "skip"; ?>
--FILE--
<?php
$schema = <<<EOT
{
  "properties": {"foo": {}, "bar": {}},
  "patternProperties": { "^v": {} },
  "additionalProperties": false
}
EOT;
$validate = new Ucl\Validate($schema);
var_dump($validate->valid('{"foo": 1}'));
var_dump($validate->valid('{"foo" : 1, "bar" : 2, "quux" : "boom"}'));
var_dump($validate->valid('[1, 2, 3]'));
//var_dump($validate->valid('{"foo":1, "vroom": 2}')); //FIXME: true?

$schema = <<<EOT
{
  "properties": {"foo": {}, "bar": {}},
  "additionalProperties": {"type": "boolean"}
}
EOT;
$validate = new Ucl\Validate($schema);
var_dump($validate->valid('{"foo": 1}'));
var_dump($validate->valid('{"foo" : 1, "bar" : 2, "quux" : true}'));
var_dump($validate->valid('{"foo" : 1, "bar" : 2, "quux" : 12}'));

$schema = '{"properties": {"foo": {}, "bar": {}}}';
$validate = new Ucl\Validate($schema);
var_dump($validate->valid('{"foo": 1, "bar": 2, "quux": true}'));
?>
--EXPECT--
bool(true)
bool(false)
bool(true)
bool(true)
bool(true)
bool(false)
bool(true)
