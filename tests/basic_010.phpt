--TEST--
basic #10
--SKIPIF--
<?php if (!extension_loaded("ucl")) print "skip"; ?>
--FILE--
<?php
$in = __DIR__ . '/basic/10.in';
$out = array(
  'section1' => array(
    'foo' => array(
      'bar' => array(
        'key' => 'value',
      ),
    ),
  ),
  'section2' => array(
    'foo' => array(
      'baz' => array(
        'key' => 'value',
      ),
    ),
  ),
  'section3' => array(
    'foo' => array(
      'bar' => 'lol',
    ),
  ),
);

$res = Ucl\load(file_get_contents($in));
var_dump($res === $out);

$res = Ucl\loadFile($in);
var_dump($res === $out);

$config = new Ucl\Config();
$config->add(file_get_contents($in));
$res = $config->parse();
var_dump($res === $out);

$config = new Ucl\Config();
$config->addFile($in);
$res = $config->parse();
var_dump($res === $out);
?>
--EXPECT--
bool(true)
bool(true)
bool(true)
bool(true)
