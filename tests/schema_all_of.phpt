--TEST--
schema all of
--SKIPIF--
<?php if (!extension_loaded("ucl")) print "skip"; ?>
--FILE--
<?php
$schema = <<<EOT
{
  "allOf": [
    {
      "properties": {
        "bar": {"type": "integer"}
      },
      "required": ["bar"]
    },
    {
      "properties": {
        "foo": {"type": "string"}
      },
      "required": ["foo"]
    }
  ]
}
EOT;
$validate = new Ucl\Validate($schema);
var_dump($validate->valid('{"foo": "baz", "bar": 2}'));
var_dump($validate->valid('{"foo": "baz"}'));
var_dump($validate->valid('{"bar": 2}'));
var_dump($validate->valid('{"foo": "baz", "bar": "quux"}'));

$schema = <<<EOT
{
  "properties": {"bar": {"type": "integer"}},
  "required": ["bar"],
  "allOf" : [
    {
      "properties": {
         "foo": {"type": "string"}
      },
      "required": ["foo"]
    },
    {
      "properties": {
        "baz": {"type": "null"}
      },
      "required": ["baz"]
    }
  ]
}
EOT;
$validate = new Ucl\Validate($schema);
var_dump($validate->valid('{"foo": "quux", "bar": 2, "baz": null}'));
var_dump($validate->valid('{"foo": "quux", "baz": null}'));
var_dump($validate->valid('{"bar": 2, "baz": null}'));
var_dump($validate->valid('{"foo": "quux", "bar": 2}'));
var_dump($validate->valid('{"bar": 2}'));

/*
FIXME:
$schema = <<<EOT
{
  "allOf": [
    {"maximum": 30},
    {"minimum": 20}
  ]
}
EOT;
$validate = new Ucl\Validate($schema);
var_dump($validate->valid('25'));
var_dump($validate->valid('35'));
*/
?>
--EXPECT--
bool(true)
bool(false)
bool(false)
bool(false)
bool(true)
bool(false)
bool(false)
bool(false)
bool(false)
