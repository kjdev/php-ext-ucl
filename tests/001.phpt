--TEST--
Check for ucl presence
--SKIPIF--
<?php if (!extension_loaded("ucl")) print "skip"; ?>
--FILE--
<?php
echo "ucl extension is available";
?>
--EXPECT--
ucl extension is available
