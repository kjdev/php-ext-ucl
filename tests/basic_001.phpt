--TEST--
basic #1
--SKIPIF--
<?php if (!extension_loaded("ucl")) print "skip"; ?>
--FILE--
<?php
$in = __DIR__ . '/basic/1.in';
$out = array(
  'key1' => 'value',
  'key2' => 'value2',
  'key3' => 'value;',
  'key4' => 1.0,
  'key5' => -3735928559,
  'key6' => '0xdeadbeef.1',
  'key7' => '0xreadbeef',
  'key8' => -1e-10,
  'key9' => 1,
  'key10' => true,
  'key11' => false,
  'key12' => true,
);

$res = Ucl\load(file_get_contents($in));
var_dump($res === $out);

$res = Ucl\loadFile($in);
var_dump($res === $out);

$config = new Ucl\Config();
$config->add(file_get_contents($in));
$res = $config->parse();
var_dump($res === $out);

$config = new Ucl\Config();
$config->addFile($in);
$res = $config->parse();
var_dump($res === $out);
?>
--EXPECT--
bool(true)
bool(true)
bool(true)
bool(true)
