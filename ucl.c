
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "Zend/zend_exceptions.h"
#include "php_ucl.h"
#include "ucl_util.h"
#include "ucl_config.h"
#include "ucl_validate.h"

ZEND_DECLARE_MODULE_GLOBALS(ucl)

ZEND_BEGIN_ARG_INFO_EX(arginfo_ucl_load, 0, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, str)
  ZEND_ARG_INFO(0, flags)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_ucl_load_file, 0, ZEND_RETURN_VALUE, 1)
  ZEND_ARG_INFO(0, filename)
  ZEND_ARG_INFO(0, flags)
ZEND_END_ARG_INFO()

PHP_UCL_FUNCTION(load)
{
  char *str = NULL;
#ifdef ZEND_ENGINE_3
  size_t str_len;
#else
  int str_len;
#endif
  long flags = 0;
  struct ucl_parser *parser;
  ucl_object_t *obj;

  if (zend_parse_parameters(ZEND_NUM_ARGS(), "s|l",
                            &str, &str_len, &flags) == FAILURE) {
    return;
  }

  if (!str || str_len == 0) {
    return;
  }

  parser = ucl_parser_new(flags);

  ucl_parser_add_chunk(parser, (const unsigned char *)str, str_len);

  if (ucl_parser_get_error(parser)) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC,
                            "%s", ucl_parser_get_error(parser));
    ucl_parser_free(parser);
    return;
  }

  obj = ucl_parser_get_object(parser);
  ucl_parser_free(parser);

  php_ucl_iterate_valid_ucl(obj, return_value);

  ucl_object_unref(obj);
}

PHP_UCL_FUNCTION(loadFile)
{
  char *filename = NULL;
#ifdef ZEND_ENGINE_3
  size_t filename_len;
#else
  int filename_len;
#endif
  long flags = 0;
  struct ucl_parser *parser;
  ucl_object_t *obj;

  if (zend_parse_parameters(ZEND_NUM_ARGS(), "s|l",
                            &filename, &filename_len, &flags) == FAILURE) {
    return;
  }

  if (!filename || filename_len == 0) {
    return;
  }

  parser = ucl_parser_new(flags);

  if (!php_ucl_parser_add_chunk_file(parser, filename)) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC,
                            "%s", ucl_parser_get_error(parser));
    return;
  }

  if (ucl_parser_get_error(parser)) {
    zend_throw_exception_ex(NULL, 0 TSRMLS_CC,
                            "%s", ucl_parser_get_error(parser));
    ucl_parser_free(parser);
    return;
  }

  obj = ucl_parser_get_object(parser);
  ucl_parser_free(parser);

  php_ucl_iterate_valid_ucl(obj, return_value);

  ucl_object_unref(obj);
}

PHP_MINIT_FUNCTION(ucl)
{
  PHP_UCL_LONG_CONSTANT("PARSER_KEY_LOWERCASE", UCL_PARSER_KEY_LOWERCASE);
  /* PHP_UCL_LONG_CONSTANT("PARSER_ZEROCOPY", UCL_PARSER_ZEROCOPY); */
  PHP_UCL_LONG_CONSTANT("PARSER_NO_TIME", UCL_PARSER_NO_TIME);
  PHP_UCL_LONG_CONSTANT("PARSER_NO_IMPLICIT_ARRAYS",
                        UCL_PARSER_NO_IMPLICIT_ARRAYS);

  PHP_UCL_LONG_CONSTANT("EMIT_JSON", UCL_EMIT_JSON);
  PHP_UCL_LONG_CONSTANT("EMIT_JSON_COMPACT", UCL_EMIT_JSON_COMPACT);
  PHP_UCL_LONG_CONSTANT("EMIT_CONFIG", UCL_EMIT_CONFIG);
  PHP_UCL_LONG_CONSTANT("EMIT_YAML", UCL_EMIT_YAML);

  php_ucl_config_class_register(TSRMLS_C);
  php_ucl_validate_class_register(TSRMLS_C);

  return SUCCESS;
}

PHP_MSHUTDOWN_FUNCTION(ucl)
{
  return SUCCESS;
}

PHP_MINFO_FUNCTION(ucl)
{
  php_info_print_table_start();
  php_info_print_table_header(2, "ucl support", "enabled");
  php_info_print_table_header(2, "extension version", PHP_UCL_VERSION);
  php_info_print_table_end();
}

const zend_function_entry ucl_functions[] = {
  PHP_UCL_FE(load, arginfo_ucl_load)
  PHP_UCL_FE(loadFile, arginfo_ucl_load_file)
  PHP_UCL_FALIAS(load_file, loadFile, arginfo_ucl_load_file)
  PHP_FE_END
};

zend_module_entry ucl_module_entry = {
  STANDARD_MODULE_HEADER,
  "ucl",
  ucl_functions,
  PHP_MINIT(ucl),
  PHP_MSHUTDOWN(ucl),
  NULL,
  NULL,
  PHP_MINFO(ucl),
  PHP_UCL_VERSION,
  STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_UCL
#ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE();
#endif
ZEND_GET_MODULE(ucl)
#endif
