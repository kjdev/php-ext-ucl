
#ifndef PHP_UCL_VALIDATE_H
#define PHP_UCL_VALIDATE_H

#ifdef ZEND_ENGINE_3
#include <ext/standard/php_smart_string.h>
#else
#include <ext/standard/php_smart_str.h>
#endif

#include "php_ucl.h"
#include "ucl_util.h"

typedef struct {
#ifndef ZEND_ENGINE_3
  zend_object std;
  smart_str error;
#endif
  ucl_object_t *schema;
#ifdef ZEND_ENGINE_3
  smart_string error;
  zend_object std;
#endif
} php_ucl_validate_t;

extern PHP_UCL_API zend_class_entry *php_ucl_validate_ce;

PHP_UCL_API int php_ucl_validate_class_register(TSRMLS_D);

#endif
