
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/file.h"
#include "php_ucl.h"
#include "ucl_util.h"

static inline void
php_ucl_object_tozval(const ucl_object_t *obj, zval *zv)
{
  if (obj->type == UCL_INT) {
    ZVAL_LONG(zv, ucl_object_toint(obj));
  } else if (obj->type == UCL_FLOAT) {
    ZVAL_DOUBLE(zv, ucl_object_todouble(obj));
  } else if (obj->type == UCL_STRING) {
#ifdef ZEND_ENGINE_3
    ZVAL_STRING(zv, ucl_object_tostring(obj));
#else
    ZVAL_STRING(zv, ucl_object_tostring(obj), 1);
#endif
  } else if (obj->type == UCL_BOOLEAN) {
    if (ucl_object_toboolean(obj)) {
      ZVAL_TRUE(zv);
    } else {
      ZVAL_FALSE(zv);
    }
  } else if (obj->type == UCL_TIME) {
    ZVAL_DOUBLE(zv, ucl_object_todouble(obj));
  } else if (obj->type == UCL_OBJECT) {
    const ucl_object_t *tmp;
    ucl_object_iter_t it = NULL;

    array_init(zv);

    while ((tmp = ucl_iterate_object(obj, &it, true))) {
      const char *key = ucl_object_key(tmp);
      if (key) {
#ifdef ZEND_ENGINE_3
        zval val;
        php_ucl_object_tozval(tmp, &val);
        zend_hash_str_add(Z_ARRVAL_P(zv), key, strlen(key), &val);
#else
        zval *val;
        MAKE_STD_ZVAL(val);
        php_ucl_object_tozval(tmp, val);
        zend_hash_add(Z_ARRVAL_P(zv), key, strlen(key) + 1,
                      &val, sizeof(zval*), NULL);
#endif
      }
    }
  } else if (obj->type == UCL_ARRAY) {
    const ucl_object_t *tmp;
    ucl_object_iter_t it = NULL;

    array_init(zv);

    while ((tmp = ucl_iterate_object(obj, &it, true))) {
#ifdef ZEND_ENGINE_3
      zval val;
      php_ucl_object_tozval(tmp, &val);
      zend_hash_next_index_insert(Z_ARRVAL_P(zv), &val);
#else
      zval *val;
      MAKE_STD_ZVAL(val);
      php_ucl_object_tozval(tmp, val);
      zend_hash_next_index_insert(Z_ARRVAL_P(zv), &val, sizeof(zval*), NULL);
#endif
    }
  } else if (obj->type == UCL_USERDATA) {
#ifdef ZEND_ENGINE_3
    ZVAL_STRINGL(zv, obj->value.ud, obj->len);
#else
    ZVAL_STRINGL(zv, obj->value.ud, obj->len, 1);
#endif
  } else {
    ZVAL_NULL(zv);
  }
}


PHP_UCL_API void
php_ucl_iterate_valid_ucl(const ucl_object_t *obj, zval *zv)
{
  const ucl_object_t *tmp;
  ucl_object_iter_t it = NULL;

  array_init(zv);

  while ((tmp = ucl_iterate_object(obj, &it, true))) {
    const char *key = ucl_object_key(tmp);
    if (key) {
#ifdef ZEND_ENGINE_3
      zval val;
      php_ucl_object_tozval(tmp, &val);
      zend_hash_str_add(Z_ARRVAL_P(zv), key, strlen(key), &val);
#else
      zval *val;
      MAKE_STD_ZVAL(val);
      php_ucl_object_tozval(tmp, val);
      zend_hash_add(Z_ARRVAL_P(zv), key, strlen(key) + 1,
                    &val, sizeof(zval*), NULL);
#endif
    } else {
#ifdef ZEND_ENGINE_3
      zval val;
      php_ucl_object_tozval(tmp, &val);
      zend_hash_next_index_insert(Z_ARRVAL_P(zv), &val);
#else
      zval *val;
      MAKE_STD_ZVAL(val);
      php_ucl_object_tozval(tmp, val);
      zend_hash_next_index_insert(Z_ARRVAL_P(zv), &val, sizeof(zval*), NULL);
#endif
    }
  }
}

PHP_UCL_API int
php_ucl_parser_add_chunk_file(struct ucl_parser *parser, char *filename)
{
  zval *zcontext = NULL;
  php_stream_context *context = NULL;
  zend_string *contents;
  php_stream *stream;
  int retval = 0;

  context = php_stream_context_from_zval(zcontext, 0);
  stream = php_stream_open_wrapper_ex(filename, "rb", REPORT_ERRORS,
                                      NULL, context);
  if (stream) {
    php_stream_seek(stream, -1, SEEK_SET);
    contents = php_stream_copy_to_mem(stream, PHP_STREAM_COPY_ALL, 0);
    if (contents) {
      ucl_parser_add_chunk(parser, (const unsigned char *)ZSTR_VAL(contents),
                           ZSTR_LEN(contents));
      zend_string_release(contents);
      retval = 1;
    }
    php_stream_close(stream);
  }

  return retval;
}
